<?php

class Enlaces {

//Propiedades
  private $id;
  private $nombre;
  private $url;

//Constructor

  public function __construct($id, $nombre, $url) {
    $this->id = $id;
    $this->nombre = $nombre;
    $this->url = $url;
  }

//Metodos
  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getNombre() {
    return $this->nombre;
  }

  public function setNombre($nombre) {
    $this->nombre = $nombre;
  }

  public function getUrl() {
    return $this->url;
  }

  public function setUrl($url) {
    $this->url = $url;
  }

}

?>
